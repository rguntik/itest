# To init project you must to run this commands in main project folder:

* composer update
* php bin/console d:mig:m
* php bin/console doctrine:fixtures:load --append
* php bin/console asset:install --symlink

## Site info:

* user: admin
* pass: admin

Description
-------------
##### On the main project page you can find:

Admin panel link

Rest api documentation link

For api authorisation you can use default token(you can find it on swagger page) or you can run command "vendor/bin/simple-phpunit" and get token from there
