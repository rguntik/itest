<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TransactionAdmin extends AbstractAdmin
{
    protected $perPageOptions = [5, 10, 50];
    protected $maxPerPage = 50;

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('amount')
            ->add('createAt', 'doctrine_orm_datetime_range', ['field_type'=>'sonata_type_datetime_range_picker'])
            ->add('updateAt', 'doctrine_orm_datetime_range', ['field_type'=>'sonata_type_datetime_range_picker'])
            ->add('customer')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('amount')
            ->add('createAt')
            ->add('updateAt')
            ->add('customer')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, ['disabled'  => true])
            ->add('amount')
            ->add('createAt', 'sonata_type_date_picker')
            ->add('updateAt', 'sonata_type_date_picker')
            ->add('customer', null, ['disabled'  => true])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('amount')
            ->add('createAt')
            ->add('updateAt')
            ->add('customer')
        ;
    }
}
