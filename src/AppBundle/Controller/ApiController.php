<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Customer;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Delete;
use AppBundle\Entity\Transaction;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ApiController extends FOSRestController
{
    /**
     * @Get("/api")
     */
    public function indexAction()
    {
        $data = ["test" => "done"];
        $view = $this->view($data);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     statusCodes={200="success"},
     *     cache=false,
     *     section="Transactions Array",
     *     requirements={
     *          {
     *              "name"="customerId",
     *              "default"="1"
     *           },
     *          {
     *              "name"="amount",
     *              "default"="100"
     *           },
     *          {
     *              "name"="date",
     *              "default"="2017-10-30"
     *           },
     *          {
     *              "name"="offset",
     *              "default"="1"
     *           },
     *          {
     *              "name"="limit",
     *              "default"="5",
     *           },
     *     },
     *     headers={
     *          {
     *              "name"="Authorization",
     *              "default"="Bearer MDlhYmQ2MTdiN2RiMTFlMTk2MWEyYzEzNTY4YTU2MDczMzViNzhlMTI1MWNhY2Q3MTVhYTM4ZTNlYzQ1MGEzYg"
     *          }
     *     }
     * )
     * @Get("/api/transactions")
     * @Get("/api/transactions/{customerId}")
     * @Get("/api/transactions/{customerId}/{amount}")
     * @Get("/api/transactions/{customerId}/{amount}/{date}")
     * @Get("/api/transactions/{customerId}/{amount}/{date}/{offset}")
     * @Get(
     *     "/api/transactions/{customerId}/{amount}/{date}/{offset}/{limit}",
     *     requirements={"amount": "\d+|-\d+", "customerId": "\d+", "offset": "\d+", "limit": "\d+"}
     * )
     * @ParamConverter("date", options={"format": "Y-m-d"})
     */
    public function transactionWithFilterAction($customerId = null, $amount = null, \DateTime $date = null, $offset = null, $limit = null)
    {
        $transaction = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(Transaction::class)
            ->getWithFilter($customerId, $amount, $date, $offset, $limit)
        ;

        $view = $this->view($transaction);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     statusCodes={200="success"},
     *     cache=false,
     *     section="Transaction",
     *     requirements={
     *          {
     *              "name"="customerId",
     *              "default"="6"
     *           },
     *          {
     *              "name"="transactionId",
     *              "default"="2",
     *           },
     *     },
     *     headers={
     *          {
     *              "name"="Authorization",
     *              "default"="Bearer MDlhYmQ2MTdiN2RiMTFlMTk2MWEyYzEzNTY4YTU2MDczMzViNzhlMTI1MWNhY2Q3MTVhYTM4ZTNlYzQ1MGEzYg"
     *          }
     *     }
     * )
     * @Get(
     *     "/api/transaction/{customerId}/{transactionId}",
     *     requirements={"customerId": "\d+", "transactionId": "\d+"}
     * )
     */
    public function transactionAction($customerId, $transactionId)
    {
        $data = null;
        $transaction = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(Transaction::class)
            ->findOneBy([
                'id' => $transactionId,
            ])
        ;

        if ($transaction && $transaction->getCustomer()->getId() == $customerId) {
            $data = $this->transactionToArray($transaction);
        }

        $view = $this->view($data);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     statusCodes={200="success"},
     *     cache=false,
     *     section="Transaction",
     *     requirements={
     *          {
     *              "name"="transaction",
     *              "default"="2",
     *           },
     *          {
     *              "name"="amount",
     *              "default"="666"
     *           },
     *     },
     *     headers={
     *          {
     *              "name"="Authorization",
     *              "default"="Bearer MDlhYmQ2MTdiN2RiMTFlMTk2MWEyYzEzNTY4YTU2MDczMzViNzhlMTI1MWNhY2Q3MTVhYTM4ZTNlYzQ1MGEzYg"
     *          }
     *     }
     * )
     * @Patch(
     *     "/api/transaction/{transaction}/{amount}",
     *     requirements={"amount": "\d+|-\d+"}
     * )
     */
    public function updateTransaction(Transaction $transaction, $amount)
    {
        $em = $this->getDoctrine()->getManager();
        $transaction->setAmount($amount);
        $em->persist($transaction);
        $em->flush();

        $view = $this->view($this->transactionToArray($transaction));

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     statusCodes={200="success"},
     *     cache=false,
     *     section="Transaction",
     *     requirements={
     *          {
     *              "name"="customer",
     *              "default"="2",
     *           },
     *          {
     *              "name"="amount",
     *              "default"="666"
     *           },
     *     },
     *     headers={
     *          {
     *              "name"="Authorization",
     *              "default"="Bearer MDlhYmQ2MTdiN2RiMTFlMTk2MWEyYzEzNTY4YTU2MDczMzViNzhlMTI1MWNhY2Q3MTVhYTM4ZTNlYzQ1MGEzYg"
     *          }
     *     }
     * )
     * @Post(
     *     "/api/transaction/{customer}/{amount}",
     *     requirements={"customer": "\d+", "amount": "\d+"}
     * )
     */
    public function addTransactionAction(Customer $customer, $amount)
    {
        $transaction = new Transaction();
        $transaction
            ->setCustomer($customer)
            ->setAmount($amount)
        ;

        $em = $this->getDoctrine()->getManager();
        $em->persist($transaction);
        $em->flush();

        $transaction = $this->transactionToArray($transaction);

        $view = $this->view($transaction);

        return $this->handleView($view);
    }

    /**
    /**
     * @ApiDoc(
     *     statusCodes={200="success"},
     *     cache=false,
     *     section="Transaction",
     *     requirements={
     *          {
     *              "name"="transaction",
     *              "default"="1000"
     *           },
     *     },
     *     headers={
     *          {
     *              "name"="Authorization",
     *              "default"="Bearer MDlhYmQ2MTdiN2RiMTFlMTk2MWEyYzEzNTY4YTU2MDczMzViNzhlMTI1MWNhY2Q3MTVhYTM4ZTNlYzQ1MGEzYg"
     *          }
     *     }
     * )
     * @Delete(
     *     "/api/transaction/{transaction}"
     * )
     */
    public function deleteTransactionAction(Transaction $transaction)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($transaction);
        $em->flush();

        $view = $this->view([]);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     statusCodes={200="success", 400="Customer name is empty"},
     *     cache=false,
     *     section="Customer",
     *     input="AppBundle\Form\CustomerType",
     *     headers={
     *          {
     *              "name"="Authorization",
     *              "default"="Bearer MDlhYmQ2MTdiN2RiMTFlMTk2MWEyYzEzNTY4YTU2MDczMzViNzhlMTI1MWNhY2Q3MTVhYTM4ZTNlYzQ1MGEzYg"
     *          }
     *     }
     * )
     * @Post("/api/customer")
     */
    public function addCustomer(Request $request)
    {
        $customer = new Customer();
        $form = $this->createForm('AppBundle\Form\CustomerType', $customer);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();
        } else {
            throw new HttpException(400, "Customer name is empty");
        }

        $view = $this->view($customer);

        return $this->handleView($view);
    }

    /**
     * @param Transaction $transaction
     * @return array
     */
    private function transactionToArray(Transaction $transaction)
    {
        return [
            'transactionId' => $transaction->getId(),
            'amount' => $transaction->getAmount(),
            'date' => $transaction->getUpdateAt()->format(\DateTime::RFC3339),
            'customerId' => $transaction->getCustomer()->getId()
        ];
    }
}
