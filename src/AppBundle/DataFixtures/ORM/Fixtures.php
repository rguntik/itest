<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\{
    Customer,
    Transaction
};

class Fixtures extends Fixture
{
    public function load(ObjectManager $em)
    {
        $nameArr = [
            ['Ivan', 'Petr', 'Vasiliy'],
            ['Ivanow', 'Petrov', 'Vasiliev']
        ];

        $customers = [];
        foreach ($nameArr[0] as $name) {
            foreach ($nameArr[1] as $surname) {
                $customer = new Customer();
                $customer->setName($name . ' ' . $surname);
                $em->persist($customer);
                $em->flush();
                $customers[] = $customer;
            }
        }

        $transactionsCount = 2000;
        $nowDate = new \DateTime();
        for ($i = 0; $i < $transactionsCount; $i++) {
            $transaction = new Transaction();
            $date = clone $nowDate;
            $amount = rand(-1000, 1000);
            $dayVal = rand(1, 30);
            $date->setTime(rand(1, 23), rand(0, 59))->modify("- {$dayVal} day");
            $transaction
                ->setCreateAt($date)
                ->setUpdateAt($date)
                ->setAmount($amount)
                ->setCustomer($customers[rand(0, count($customers) - 1)])
            ;

            $em->persist($transaction);
            $em->flush();
        }
    }
}
