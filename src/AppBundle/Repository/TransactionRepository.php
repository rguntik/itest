<?php

namespace AppBundle\Repository;

class TransactionRepository extends \Doctrine\ORM\EntityRepository
{
    public function getWithFilter($customerId = null, $amount = null, \DateTime $date = null, $offset = null, $limit = null)
    {
        $query = $this
            ->createQueryBuilder('t')
            ->select('t.id AS transactionId')
            ->addSelect([
                't.amount',
                "DATE_FORMAT(t.updateAt, '%Y-%m-%d') AS date",
                "DATE_FORMAT(t.updateAt, '%H:%i:%s') AS time",
                "c.id AS customerId",
            ])
            ->join('t.customer', 'c')
            ->orderBy('t.id')
        ;

        if ($customerId) {
            $query->andWhere('t.customer = :customerId')->setParameter('customerId', $customerId);
        }

        if ($amount) {
            $query->andWhere('t.amount = :amount')->setParameter('amount', $amount);
        }

        if ($date) {
            $query
                ->andWhere("t.updateAt BETWEEN DATE_SUB(:date, 1, 'DAY') AND :date")
                ->setParameter('date', $date->setTime(0, 0)->modify('+ 1 day'))
            ;
        }

        if ($offset) {
            $query->setFirstResult($offset);
        }

        if ($limit) {
           $query->setMaxResults($limit);
        }

        $result = $query->getQuery()->getArrayResult();

        return $result;
    }
}
