<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Entity\{
    Transaction,
    Client,
    User
};

class ApiControllerTest extends WebTestCase
{
    private $container;
    private $em;
    private $header;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        static::bootKernel();
        $this->container = static::$kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();

        parent::__construct($name, $data, $dataName);
    }

    public function testOauth()
    {
        $client = static::createClient();
        $client->request('GET', '/api');
        $this->assertEquals(401, $client->getResponse()->getStatusCode());

        $oAuthClient = $this->em->getRepository(Client::class)->findAll();

        $this->assertNotEquals(0, count($oAuthClient), 'Client table is empty');

        /** @var Client $oAuthClient */
        $oAuthClient = reset($oAuthClient);

        $client->request('POST', '/oauth/v2/token', [
            'grant_type' => 'password',
            'client_id' => $oAuthClient->getPublicId(),
            'client_secret' => $oAuthClient->getSecret(),
            'username' => 'admin',
            'password' => 'admin',
        ]);

        $status = $client->getResponse()->getStatusCode();
        $this->assertEquals(200, $status);
        $tokenContent = json_decode($client->getResponse()->getContent());

        print_r("\n++++++++++++++++++++++++++++++++++++++++++++++access token++++++++++++++++++++++++++++++++++++++++++\n\n");
        print_r("access_token: " . $tokenContent->access_token);
        print_r("\n\n ++++++++++++++++++++++++++++++++++++++++++++++access token++++++++++++++++++++++++++++++++++++++++++\n\n");

        $token = json_decode($client->getResponse()->getContent());
        $this->header = [
            'HTTP_AUTHORIZATION' => 'Bearer ' . $token->access_token,
            'CONTENT_TYPE' => 'application/json',
        ];
        $client->request('GET', '/api', [], [], $this->header);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
